package com.SYD;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by insonix on 21/4/16.
 */
public class Login extends Activity {
    Button login_button;
    TextView register1;
    private TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutPassword;
    public static EditText etUserName, etPassword,etName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        login_button=(Button)findViewById(R.id.login_button);
        register1=(TextView)findViewById(R.id.register1);
        etUserName=(EditText)findViewById(R.id.editUsername);
        etPassword=(EditText)findViewById(R.id.editPasswd);
        etName=(EditText)findViewById(R.id.input_name);
        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable() == false) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                    builder.setMessage("Error in Network Connection")
                            .setIcon(R.drawable.alert)
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("Alert");
                    alert.show();
                    //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
                }
//                else if(etName.getText().toString().length() == 0){
//                    inputLayoutName.setError(getString(R.string.err_msg_name));
//                    etName.requestFocus();
//                }

                else if(etUserName.getText().toString().length() == 0){
                    etUserName.setError(getString(R.string.err_msg_name));
                    etUserName.requestFocus();
                }
//                else if(!emailAddress.getText().toString().matches(emailPattern)){
//                    emailAddress.setError("Invaild email!");
//                }
                else if(etPassword.getText().toString().length() == 0){
                    etPassword.setError("Password is required!");
                }
                else{
                    Intent i= new Intent(Login.this, HomeScreen.class);
                    startActivity(i);
                    overridePendingTransition(0,0);
                    finish();
                }

            }
        });
        register1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Register.class);
                startActivity(i);
                overridePendingTransition(0,0);
                finish();
            }
        });
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
