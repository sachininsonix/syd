package com.SYD;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;


/**
 * Created by mohit on 22-Feb-16.
 */
public class Splash_screen extends Activity  {
    /** Duration of wait **/

    ImageView tvMuvText;

    SharedPreferences preferences;

GPSTracker gpsTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        gpsTracker = new GPSTracker(Splash_screen.this);
        final NetworkInfo activeNetwork;
        ConnectivityManager cm =
                (ConnectivityManager) Splash_screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


//
//
//        if (isConnected == true) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);

                        if (Build.VERSION.SDK_INT >= 23) {
                            if (ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED
                                    || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.VIBRATE)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.INTERNET)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.ACCESS_WIFI_STATE)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.ACCESS_NETWORK_STATE)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.MANAGE_DOCUMENTS)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.WAKE_LOCK)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.READ_PHONE_STATE)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.MODIFY_PHONE_STATE)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.READ_CONTACTS)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.SEND_SMS)
                                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.DISABLE_KEYGUARD)
                                    != PackageManager.PERMISSION_GRANTED

                                    ) {
                                ActivityCompat.requestPermissions(Splash_screen.this,
                                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                                android.Manifest.permission.VIBRATE, android.Manifest.permission.INTERNET,
                                                android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                                                android.Manifest.permission.ACCESS_WIFI_STATE, android.Manifest.permission.ACCESS_NETWORK_STATE,
                                                android.Manifest.permission.MANAGE_DOCUMENTS, android.Manifest.permission.CALL_PHONE, android.Manifest.permission.WAKE_LOCK,
                                                android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.MODIFY_PHONE_STATE, android.Manifest.permission.READ_CONTACTS
                                                , android.Manifest.permission.SEND_SMS, android.Manifest.permission.DISABLE_KEYGUARD},
                                        1);


                            }
                        }else {

                        }
                        Intent i = new Intent(Splash_screen.this, MaLogin.class);
                        startActivity(i);

                        //Remove activity
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();

//        }

//    else {
//            Toast.makeText(Splash_screen.this, " No Internet connection ", Toast.LENGTH_LONG).show();
//        }
    }
    @Override protected void onResume()
    {
        super.onResume();
//        checkPlayServices();
    }



    @Override
    protected void onStop() {
        super.onStop();


    }
    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}
