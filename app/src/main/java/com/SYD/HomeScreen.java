package com.SYD;

/**
 * Created by insonix on 13/4/16.
 */
import java.io.IOException;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class HomeScreen extends Activity implements IBaseGpsListener {
    Button blockcall;
    private SharedPreferences myPrefs;
    private SharedPreferences.Editor editor;

    private RadioButton allBlock;
    private RadioButton unSaved;
    private RadioButton fromList;
    private RadioButton cancelAll;
    //
    TextView incomingNo,latit,longit,provi;
    private Button btnAddNumber;
    private Button btnShowList,btnsend;
    public static HomeScreen ins;
    String longitude,latitude,address,locateCurrent;
    Context context;
    double lati,longi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainhome);
        myPrefs = this.getSharedPreferences("myPrefs", MODE_WORLD_READABLE);
        editor=myPrefs.edit();
        ins=this;
//        blockcall=(Button)findViewById(R.id.blockcall);
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        this.updateSpeed(null);
//        latit=(TextView)findViewById(R.id.latit);
//        longit=(TextView)findViewById(R.id.longit);
//        provi=(TextView)findViewById(R.id.provi);
        /*--Start Service--*/
        // Method to start the service
//        startService(new Intent(HomeScreen.this, LocationService.class));

//       // TO diable the Home Button
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        // TO diable the Power Button
//        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
//        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
//        lock.disableKeyguard();


        try {
    String sppeed=getIntent().getExtras().getString("speed");
//    Toast.makeText(HomeScreen.this,sppeed,Toast.LENGTH_SHORT).show();
    if (sppeed.equalsIgnoreCase("speedabove")){
//        latit.setText("Speed above 30 km/hr ");
    }
    else if(sppeed.equalsIgnoreCase("speed")){
//        latit.setText("Normal speed ");
    }
    else{
//        latit.setText("Constant ");
    }
//    latitude = getIntent().getExtras().getString("speed");
//    Longitude = getIntent().getExtras().getString("Longitude");
//    address = getIntent().getExtras().getString("Provider");
//    latit.setText(latitude);
//    longit.setText(Longitude);
//    provi.setText(address);
}catch (Exception e){
    e.printStackTrace();
}
        CheckBox chkUseMetricUntis = (CheckBox) this.findViewById(R.id.chkMetricUnits);
        chkUseMetricUntis.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                HomeScreen.this.updateSpeed(null);
            }
        });
//        blockcall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(HomeScreen.this,Blockers.class);
//                startActivity(intent);
//            }
//        });


    }
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
////Do Code Here
//// If want to block just return false
//            return false;
//        }
//        if (keyCode == KeyEvent.KEYCODE_MENU) {
////Do Code Here
//// If want to block just return false
//            return false;
//        }
//        if (keyCode == KeyEvent.KEYCODE_HOME) {
////Do Code Here
//// If want to block just return false
//            return false;
//        }
//        if (keyCode == KeyEvent.KEYCODE_SEARCH) {
////Do Code Here
//// If want to block just return false
//            return false;
//        }
//        if (keyCode == KeyEvent.KEYCODE_SETTINGS) {
////Do Code Here
//// If want to block just return false
//            return false;
//        }
//
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    protected void onResume(){
        super.onResume();
        // code to update the date here

        try {
            String sppeed=getIntent().getExtras().getString("speed");
//            Toast.makeText(HomeScreen.this,sppeed,Toast.LENGTH_SHORT).show();
            if (sppeed.equalsIgnoreCase("speedabove")){
//                latit.setText("Speed above 30 km/hr ");
            }
            else if(sppeed.equalsIgnoreCase("speed")){
//                latit.setText("Normal speed ");
 }
            else{
//                latit.setText("Constant ");
            }
//    latitude = getIntent().getExtras().getString("speed");
//    Longitude = getIntent().getExtras().getString("Longitude");
//    address = getIntent().getExtras().getString("Provider");
//    latit.setText(latitude);
//    longit.setText(Longitude);
//    provi.setText(address);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
//    public void startService(View view) {
//        startService(new Intent(getBaseContext(), LocationService.class));
//    }
    public static HomeScreen  getInstace(){
        return ins;
    }

    public void finish()
    {
        super.finish();
        System.exit(0);
    }

    private void updateSpeed(CLocation location) {
        // TODO Auto-generated method stub
        float nCurrentSpeed = 0;

        if(location != null)
        {
            location.setUseMetricunits(this.useMetricUnits());
            nCurrentSpeed = location.getSpeed();
            longi = location.getLongitude();
//            Log.v(TAG, longitude);
            lati = location.getLatitude();
//            Log.v(TAG, latitude);

        /*----------to get City-Name from coordinates ------------- */
            address = null;
            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(location.getLatitude(),
                        location.getLongitude(), 1);
                if (addresses.size() > 0)
                    System.out.println(addresses.get(0).getLocality());
                address = addresses.get(0).getLocality();
            } catch (IOException e) {
                e.printStackTrace();
            }

            locateCurrent = longitude + "\n" + latitude + "\n\nMy Currrent City is: "
                    + address;

        }

        Formatter fmt = new Formatter(new StringBuilder());
        fmt.format(Locale.US, "%5.1f", nCurrentSpeed);
        String strCurrentSpeed = fmt.toString();
        strCurrentSpeed = strCurrentSpeed.replace(' ', '0');
        float finalSpeed =Float.parseFloat(strCurrentSpeed);
        String strUnits = "Miles/hour";
        if(this.useMetricUnits())
        {
            strUnits = "Km/h";
        }
        if(finalSpeed > 30){

            editor.putString("mode", "all");
            editor.commit();
            try {
                provi.setText(locateCurrent);
            }catch (Exception e){}
        }
//        Toast.makeText(HomeScreen.this, locateCurrent, Toast.LENGTH_SHORT).show();

//            TextView txtCurrentSpeed = (TextView) this.findViewById(R.id.txtCurrentSpeed);
//        txtCurrentSpeed.setText(strCurrentSpeed + " " + strUnits);
    }

    private boolean useMetricUnits() {
        // TODO Auto-generated method stub
        CheckBox chkUseMetricUnits = (CheckBox) this.findViewById(R.id.chkMetricUnits);
        return chkUseMetricUnits.isChecked();
    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub
        if(location != null)
        {
            CLocation myLocation = new CLocation(location, this.useMetricUnits());
            this.updateSpeed(myLocation);
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onGpsStatusChanged(int event) {
        // TODO Auto-generated method stub

    }
    public void updateTheTextView(final String t) {
        HomeScreen.this.runOnUiThread(new Runnable() {
            public void run() {
//                incomingNo = (TextView) findViewById(R.id.incomingNo);
//                incomingNo.setText(t);
//                Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + t));
//                intent1.putExtra("sms_body", "I am drivig I will call you later");
//                startActivity(intent1);
                sendSMSMessage();

            }
        });
    }
    protected void sendSMSMessage()
    {
        Log.d("Send SMS", "");
        String phoneNo = incomingNo.getText().toString().trim();
        String message = "I am Driving! I will call you later";
        try
        {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "SMS sent.",
                    Toast.LENGTH_LONG).show();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"SMS failed, please try again.",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}