package com.SYD;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by insonix on 22/4/16.
 */
public class MaRegister extends Activity {
    Button login_button;
    TextView register1;
    private TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutPassword,inputLayoutcPassword,inputLayoutMobile;
    public static EditText etUserName, etPassword,etCpassword,etmobile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registr);
        login_button=(Button)findViewById(R.id.btn_signup);
        register1=(TextView)findViewById(R.id.register1);
        etUserName=(EditText)findViewById(R.id.input_name);
        etPassword=(EditText)findViewById(R.id.input_password);
        etCpassword=(EditText)findViewById(R.id.input_cpassword);
        etmobile=(EditText)findViewById(R.id.input_mobile);
        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        inputLayoutcPassword = (TextInputLayout) findViewById(R.id.input_layout_cpassword);
        inputLayoutMobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable() == false) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MaRegister.this);
                    builder.setMessage("Error in Network Connection")
                            .setIcon(R.drawable.alert)
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("Alert");
                    alert.show();
                    //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
                }
                else{
                    submitForm();
                }

            }
        });
        register1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MaRegister.this, MaLogin.class);
                startActivity(i);
                overridePendingTransition(0,0);
                finish();
            }
        });
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateName()) {
            return;
        }

//        if (!validateEmail()) {
//            return;
//        }

        else if (!validatePassword()) {
            return;
        }
        else if (!validatecPassword()) {
            return;
        }
        else if (!validateMobile()) {
            return;
        }
        else {
            Intent i = new Intent(MaRegister.this, MaLogin.class);
            startActivity(i);
            Toast.makeText(MaRegister.this,"You have Registered Sucessfully!",Toast.LENGTH_SHORT).show();
            overridePendingTransition(0, 0);
            finish();
        }
    }

    private boolean validateName() {
        if (etUserName.getText().toString().trim().isEmpty()) {
            inputLayoutName.setError(getString(R.string.err_msg_name));
            requestFocus(etUserName);
            return false;
        } else {
            inputLayoutName.setErrorEnabled(false);
        }

        return true;
    }

//    private boolean validateEmail() {
//        String email = inputEmail.getText().toString().trim();
//
//        if (email.isEmpty() || !isValidEmail(email)) {
//            inputLayoutEmail.setError(getString(R.string.err_msg_email));
//            requestFocus(inputEmail);
//            return false;
//        } else {
//            inputLayoutEmail.setErrorEnabled(false);
//        }
//
//        return true;
//    }

    private boolean validatePassword() {
        if (etPassword.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(etPassword);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validatecPassword() {
        if (etCpassword.getText().toString().trim().isEmpty()) {
            inputLayoutcPassword.setError(getString(R.string.err_msg_cpassword));
            requestFocus(etCpassword);
            return false;
        }  else if(!etPassword.getText().toString().matches(etCpassword.getText().toString())) {
            inputLayoutPassword.setError("Password is mismatch!");
            inputLayoutcPassword.setError("Confirm Password is mismatch!");
            etPassword.requestFocus();
            return false;
        }else {
            inputLayoutPassword.setErrorEnabled(false);
            inputLayoutcPassword.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateMobile() {
        if (etmobile.getText().toString().trim().isEmpty()) {
            inputLayoutMobile.setError(getString(R.string.err_msg_mobile));
            requestFocus(etmobile);
            return false;
        }  else if(etmobile.getText().toString().length() < 10 || etmobile.getText().toString().length() > 10) {
            inputLayoutMobile.setError("Invaild Phone number!");
            etmobile.requestFocus();
            return false;
        }else {
            inputLayoutMobile.setErrorEnabled(false);
        }

        return true;
    }
//    private static boolean isValidEmail(String email) {
//        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
//    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
//                case R.id.input_email:
//                    validateEmail();
//                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
                case R.id.input_cpassword:
                    validatePassword();
                    break;
                case R.id.input_mobile:
                    validatePassword();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}