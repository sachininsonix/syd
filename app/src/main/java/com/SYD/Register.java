package com.SYD;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by insonix on 21/4/16.
 */
public class Register extends Activity {
    public static Button register_button;
    public static EditText etUserName, etPassword,etCPassword,etPhoneNum;
    public static TextView register1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        register_button=(Button)findViewById(R.id.register_button);
        register1=(TextView)findViewById(R.id.register1);
        etUserName = (EditText)findViewById(R.id.editUsername);
        etPassword = (EditText)findViewById(R.id.editPasswd);
        etCPassword = (EditText)findViewById(R.id.editconPasswd);
        etPhoneNum = (EditText)findViewById(R.id.editMob);
        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable() == false) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
                    builder.setMessage("Error in Network Connection")
                            .setIcon(R.drawable.alert)
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("Alert");
                    alert.show();
                    //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
                }

                else if( etUserName.getText().toString().length() == 0 ) {
                    etUserName.setError("Username is required!");

                }
                else if(etPassword.getText().toString().length() == 0 ) {
                    etPassword.setError("Password is required!");
                    etPassword.requestFocus();
                }
                else if( etCPassword.getText().toString().length() == 0 ) {
                    etCPassword.setError("Confirm Password is required!");
                    etCPassword.requestFocus();
                }
                else if(!etPassword.getText().toString().matches(etCPassword.getText().toString())) {
                    etPassword.setError("Password is mismatch!");
                    etCPassword.setError("Confirm Password is mismatch!");
                    etPassword.requestFocus();
                }
                else if( etPhoneNum.getText().toString().length() == 0 ) {
                    etPhoneNum.setError("Phone is required!");
                    etPhoneNum.requestFocus();
                }
                else if( etPhoneNum.getText().toString().length() < 10 || etPhoneNum.getText().toString().length() > 10) {
                    etPhoneNum.setError("Invaild Phone number!");
                    etPhoneNum.requestFocus();
                }
                else {
                    Intent i = new Intent(Register.this, Login.class);
                    startActivity(i);
                    overridePendingTransition(0,0);
                    finish();
                }

                Register.etPassword.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        etPassword.setError(null);
                        etCPassword.setError(null);
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                        etPassword.setError(null);
                        etCPassword.setError(null);
                    }
                });
                Register.etCPassword.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        etPassword.setError(null);
                        etCPassword.setError(null);
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        etPassword.setError(null);
                        etCPassword.setError(null);
                    }
                });

            }
        });
        register1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, Login.class);
                startActivity(i);
                overridePendingTransition(0,0);
                finish();
            }
        });
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}