package com.SYD;

/**
 * Created by insonix on 20/4/16.
 */

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by insonix on 20/4/16.
 */
public class LocationService extends Service
{
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1000; // in
    // Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 10000; // in
    // Milliseconds
    protected LocationManager locationManager;





    private static Timer timer = new Timer();
    private Context ctx;
    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public void onCreate()
    {
        super.onCreate();
        ctx = this;
        startService();
    }

    private void startService()
    {
        timer.scheduleAtFixedRate(new mainTask(), 0, 30000);
    }

    private class mainTask extends TimerTask
    {
        public void run()
        {
            toastHandler.sendEmptyMessage(0);
        }
    }

    public void onDestroy()
    {
        super.onDestroy();
        Toast.makeText(this, "Service Stopped ...", Toast.LENGTH_SHORT).show();
    }

    private final Handler toastHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
//            Intent i = new Intent(LocationService.this, HomeScreen.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//            startActivity(i);


            Toast.makeText(LocationService.this, "GPS Service created ...", Toast.LENGTH_SHORT).show();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, new MyLocationListener());
        Location location = locationManager
                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
//        float latitude = (float) location.getLatitude();
//        float longitude = (float) location.getLongitude();
//        String uniqueId = getIMEI();
        if (location != null ) {
            String message = String.format(
                    "Current Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude());
            Toast.makeText(LocationService.this, message, Toast.LENGTH_SHORT).show();
        }
        }
    };
    private class MyLocationListener implements LocationListener
    {
        public void onLocationChanged(Location location) {

            if(location.getSpeed() > 8.33333){



//                Toast.makeText(this, "Alarm set in " + seconds + " seconds", Toast.LENGTH_LONG).show();
                String message = String.format(

                                "Speed above 30 Km/h Location without Accuray " + location.getSpeed() + " \n Longitude: %1$s  \n Latitude: %2$s",
                                location.getLongitude(), location.getLatitude());
                Toast.makeText(LocationService.this, message, Toast.LENGTH_SHORT)
                        .show();
                Intent intent = new Intent(LocationService.this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("speed","speedabove");
                startActivity(intent);

            }
            else{
//                Intent intent = new Intent(LocationService.this, HomeScreen.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("speed","speed");
//                startActivity(intent);

                String message = String.format(

                        "New Location "+ location.getSpeed() +" \n Longitude: %1$s  \n Latitude: %2$s",
                        location.getLongitude(), location.getLatitude());
                Toast.makeText(LocationService.this, message, Toast.LENGTH_SHORT)
                        .show();
            }
        }

        public void onStatusChanged(String s, int i, Bundle b) {
            Toast.makeText(LocationService.this, "Provider status changed",
                    Toast.LENGTH_SHORT).show();
        }

        public void onProviderDisabled(String s) {
            Toast.makeText(LocationService.this,
                    "Provider disabled by the user. GPS turned off",
                    Toast.LENGTH_SHORT).show();
        }

        public void onProviderEnabled(String s) {
            Toast.makeText(LocationService.this,
                    "Provider enabled by the user. GPS turned on",
                    Toast.LENGTH_SHORT).show();
        }
    }
}